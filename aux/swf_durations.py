import json

import numpy as np
#import matplotlib.pyplot as plt
import plotly.express as px

from parse_swf import parse_swf, status_codes

#Index(['end_time', 'job_id', 'alt_job_id', 'host_queue', 'batch_account',
#       'allocation', 'username', 'job_name', 'job_type', 'node_count',
#       'duration', 'usage', 'exit_code', 'script', 'cwd', 'full_job_name',
#       'preemption'],

def to_int(s : str) -> int:
    if len(s) == 0:
        return 0
    return int(s)

def to_hrs(x : float) -> float:
    return x/60.0**2

def to_bin(x):
    if x >= 2765:
        return 1
    if x >= 922:
        return 2
    if x >= 92:
        return 3
    if x >= 46:
        return 4
    return 5

# This is what was used (as of 12/16/2023) to make the summit swf file.
# Values were set by associating exit codes with LSF status
# information - only counting an exit code as that status
# when the two appeared together.
def exit_status(x):
    if x == 0:
        return 1 # success
    if x == 19:
        return 4 # killed
    if x in [14, 33280, 65280]:
        return 5 # canceled
    return 0 # failed

# This was the original exit status function (11/3/2023),
# made by statistical grouping exit of codes and educated guessing.
def old_exit_status(x):
    if x == 0:
        return 0
    if abs(x) < 10:
        return 1
    if x == 19:
        return 2
    if x == 256:
        return 3
    return 4

def main(argv):
    assert len(argv) == 2, f"Usage: {argv[0]} <data.swf>"
    df = parse_swf(argv[1])
    #print(df["status"].value_counts())
    #exit(0)

    df["time_request"]  = df["time_request"].apply(to_hrs)
    df["duration"] = df["run_dt"] / 60.0**2

    df["bin"] = df["nprocs_used"].apply(to_bin)

    df["bin"] = 2*(6-df["bin"])
    print(df.iloc[0])
    #print(len(df["username"].unique()))

    #df['id'] = df.index
    #df2 = pd.melt(df, id_vars='id', value_vars=df.columns)
    fig = px.scatter(df, x="time_request", y="duration", color="status",
                     size="bin", hover_data=["nprocs_used"])
    #fig.show()
    fig.write_html("completion_times.html",
                   full_html=False, include_plotlyjs="cdn")
    #df.plot.scatter(x="script", y="duration")
    #plt.show()

    fig = px.scatter(df, x="time_request", y="nprocs_used", color="status",
                     marginal_x = "histogram", marginal_y = "histogram",
                     hover_data=["duration"])
    fig.write_html("request_sizes.html",
                   full_html=False, include_plotlyjs="cdn")
    #fig.show()

if __name__=="__main__":
    import sys
    main(sys.argv)
