from pathlib import Path

import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# Note: This includes active/inactive transitions.
#   "queued" events when user_pend_jobs+user_run_jobs == 0
#   moves from inactive -> active
#   (only queued events could occur here)
#
#   "completed/killed/canceled" events
#   when user_pend_jobs+user_run_jobs == 0
#   moves from active -> inactive
#   (only "completed/killed/canceled" or "queued" events are
#    valid here, since "running" can't happen with no pending
#    jobs).
#
#   inactive_time = df.loc[df.user_pend_jobs+df.user_run_jobs==0, "user_dt"].sum()
#   active_time   = df.loc[df.user_pend_jobs+df.user_run_jobs > 0, "user_dt"].sum()
#   active_waiting = df.loc[(df.user_pend_jobs>0)&(df.user_run_jobs==0), "user_dt"].sum()
#
"""
>>> df.loc[(df.user=="science"), ["user", "proj", "event", "user_run_jobs", "user_run_nodes", "user_dt"]]
         user    proj  ... user_run_nodes    user_dt
70    science  MAT000  ...              0   0.000000
72    science  MAT000  ...              0   0.000000
111   science  MAT000  ...              1   0.183333
1393  science  MAT000  ...              0   5.383333
1397  science  MAT000  ...              0   0.033333
1448  science  MAT000  ...              2   0.166667
5116  science  MAT000  ...              0  22.250000
5122  science  MAT000  ...              0   0.016667
5187  science  MAT000  ...              2   0.166667

[9 rows x 6 columns]
>>> df.loc[(df.proj=="MAT000"), ["user", "proj", "event", "proj_run_jobs", "proj_run_nodes", "proj_dt"]]
         user    proj  ... proj_run_nodes    proj_dt
70    science  MAT000  ...              0   0.000000
72    science  MAT000  ...              0   0.000000
111   science  MAT000  ...              1   0.183333
1393  science  MAT000  ...              0   5.383333
1397  science  MAT000  ...              0   0.033333
1448  science  MAT000  ...              2   0.166667
5116  science  MAT000  ...              0  22.250000
5122  science  MAT000  ...              0   0.016667
5187  science  MAT000  ...              2   0.166667

[9 rows x 6 columns]

>>> df.loc[(df.user=="quant"), ["user", "event", "user_run_jobs", "user_run_nodes", "user_dt"]]
        user    event  user_run_jobs  user_run_nodes     user_dt
67     quant   killed              2               2    0.166667
649    quant   killed              1               1    2.000000
695    quant   queued              0               0    0.216667
738    quant  running              0               0    0.200000
1055   quant   queued              1               1    2.066667
1057   quant  running              1               1    0.000000
1108   quant   killed              2               2    0.116667
1419   quant   killed              1               1    1.066667
1635   quant   queued              0               0    0.950000
1648   quant  running              0               0    0.066667
2089   quant   killed              1               1    2.183333
3883   quant   queued              0               0   15.850000
3890   quant  running              0               0    0.016667
4718   quant   killed              1               1    2.166667
5189   quant   queued              0               0    1.333333
5194   quant  running              0               0    0.016667
5772   quant   killed              1               1    2.166667
8820   quant   queued              0               0   19.366667
8829   quant  running              0               0    0.150000
9518   quant   killed              1               1    2.166667
52632  quant   queued              0               0  162.466667
52633  quant  running              0               0    0.000000
53349  quant   killed              1               1    2.183333
53573  quant   queued              0               0    0.183333
53601  quant  running              0               0    0.000000
54758  quant   queued              1               1    2.016667
54759  quant  running              1               1    0.000000
54806  quant   killed              2               2    0.166667
55114  quant   queued              1               1    1.933333
55121  quant  running              1               1    0.016667
55129  quant   killed              2               2    0.066667
55280  quant   killed              1               1    1.116667
"""

def read_data():
    df = pd.read_csv("events.csv", index_col=0)

    df["job_ldt"] = np.log(df["job_dt"]*60+1.0/60)/np.log(10.0)
    #df["user_run_level"] = np.minimum( ((df.user_run_jobs+9)//10).astype(int), 3)*10
    df["job_order"] = np.minimum( ((df.user_rank+9)//10).astype(int), 3)*10
    #df["user_run_level"] = np.minimum( ((df.user_run_jobs+df.user_pend_jobs+9)//10).astype(int), 3)*10
    df["capability"] = df["nodes"] >= 922

    # transition "activating" a user
    df["activate"] = (df["user_pend_jobs"] == 0)
    # transition starting a "leading" job
    df["leader"] = (df["user_run_jobs"] == 0) \
                 & (df["event"] == "running")
    df["user_jobs"] = df["user_pend_jobs"]+df["user_run_jobs"]

    # queue category
    df["bin"] = 5
    df.loc[df["nodes"] >= 46, "bin"] = 4
    df.loc[df["nodes"] >= 92, "bin"] = 3
    df.loc[df["nodes"] >= 922, "bin"] = 2
    df.loc[df["nodes"] >= 2765, "bin"] = 1


    for col in ["user_dt", "proj_dt", "sys_dt"]:
        df[col] = np.maximum(df[col], 0.0)

    return df

def findings():
    df = read_data()

    user_active  = df.loc[df.user_jobs > 0]
    #fig = px.histogram(user_active,
    #        y="user_dt",
    #        histfunc="avg",
    #        x="job_order", nbins=6)
    #if not Path("wait_times.html").exists():
    if True:
        fig = px.violin(user_active.loc[user_active["event"]=="running"],
                        x="job_order",
                        y="job_ldt",
                        color="capability",
                        hover_data="event",
                        box=True)
        #fig.show()
        fig.write_html("wait_times.html",
                       full_html=False,
                       include_plotlyjs='cdn')

    q = df.loc[df.event == "queued"] #.dropna(subset=["time"])
    q["time"] = q["time"].astype(int)
    # shift by the min-time on a 24-hr boundary
    q["time"] = q["time"] - 24*(q["time"].min()//24)

    # number of jobs queued in ea. time interval
    # also count the active projects and users at the same time
    counts = q.groupby("time").agg({
        "time": "count",
        "activate": "count",
        "active_projs": "mean",
        "active_users": "mean"})
    #print(counts)
    counts["new jobs / hr"] = counts["time"]
    print(f"avg jobs per hr = {counts.time.mean()}")
    print(f"avg active users = {counts.active_users.mean()}")
    print(f"avg active projs = {counts.active_projs.mean()}")

    #if not Path("new_jobs.html").exists():
    if True:
        fig = px.histogram(counts, x="new jobs / hr")
        #fig.show()
        fig.write_html("new_jobs.html",
                       full_html=False,
                       include_plotlyjs='cdn')

    #if not Path("jobs_users.html").exists():
    if True:
        fig = px.histogram(counts,
                         x = "active_users",
                         y = "new jobs / hr",
                         histfunc="avg")
        #fig.show()
        fig.write_html("jobs_users.html",
                       full_html=False,
                       include_plotlyjs='cdn')

    if True:
        fig = go.Figure()
        for fate in ["success", "canceled", "failed", "killed"]:
            survival = df.loc[df.event == fate, "job_dt"].values
            survival.sort()
            print(f"  {fate}: N = {len(survival)} T = {survival.sum()}")
            count = np.arange(len(survival),0,-1)/len(survival)
            fig.add_trace(go.Scatter(x=survival, y=count,
                          mode='lines',
                          name=fate))

        fig.update_xaxes(type="log")
        fig.update_yaxes(type="log")
        fig.write_html("survival.html",
                       full_html=False,
                       include_plotlyjs='cdn')
        fig.show()

    # conditional statistics vs. job rank
    #  - failure rate
    #  - number of user jobs in the queue
    #if not Path("urank_fates.html").exists():
    if True:
        done = df.loc[~df.event.isin(["queued","running"])]
        fig = px.histogram(done,
                           x=done["job_order"].apply(str),
                           labels={"x": "job_order",
                                   "y": "count"},
                           color="event")

       #fig = go.Figure()
       #for event in ["completed", "failed", "canceled", "killed"]:
       #    fig.add_trace(go.Histogram(
       #        x=df.loc[~df.capability & (df.event==event),
       #                    "job_order"],
       #        bingroup=1, xbins=xbin,
       #        name=event))
       #    fig.add_trace(go.Histogram(
       #        x=df.loc[df.capability & (df.event==event),
       #                    "job_order"],
       #        bingroup=2, xbins=xbin,
       #        name=event))
       #fig.update_layout(barmode="overlay", bargap=0.1)
        #.update_xaxes(categoryorder='total descending')
        fig.write_html("urank_fates.html",
                       full_html=False,
                       include_plotlyjs='cdn')

    #if not Path("urank_num.html").exists():
    run = df.loc[df.event == "running"]
    if True:
        fig = go.Figure()
        xbin = go.histogram.XBins(start=-5,end=40,size=10)
        fig.add_trace(go.Histogram(
            histfunc="avg",
            y=run.loc[~run.capability,"user_jobs"],
            x=run.loc[~run.capability,"job_order"],
            xbins=xbin,
            name="small"))
        fig.add_trace(go.Histogram(
            histfunc="avg",
            y=run.loc[run.capability,"user_jobs"],
            x=run.loc[run.capability,"job_order"],
            xbins=xbin,
            name="capability"))
        #.update_xaxes(categoryorder='total descending')
        fig.write_html("urank_num.html",
                       full_html=False,
                       include_plotlyjs='cdn')

    #n, bins, _ = plt.hist(counts)
    #plt.show()

    # dot-plot, #active jobs vs. rank at job start
    if True:
        #bins = np.array([1,2,3,4,5,6,7,8,9,10,11,10000])-0.5
        #G, _x, _y = np.histogram2d(run["user_rank"]+1, run["user_jobs"], bins=bins)
        #wG, _x, _y = np.histogram2d(run["user_rank"]+1, run["user_jobs"], bins=bins, weights=run["job_dt"])
        # wG /= G
        #print(G)
       #px.density_heatmap(run,
       #                   x="user_jobs", y="user_rank",
       #                   text_auto=True,
       #                   marginal_x="histogram",
       #                   marginal_y="histogram")
       #px.density_heatmap(run,
       #                   x="user_jobs", y="user_rank",
       #                   text_auto=True,
       #                   z="job_dt",
       #                   histfunc="avg",
       #                   marginal_x="histogram",
       #                   marginal_y="histogram")
        # map all larger values to 10
        vals = lambda s: np.minimum(run[s].values
                + (s=="user_rank"), 10)
        labels={"x": "active jobs",
                "y": "starting job rank"},
        fig = make_subplots(1,2, subplot_titles=(
                            "Starting Job Count",
                            "Average wait"))
        fig.add_trace(go.Histogram2d(
            x=vals("user_jobs"), y=vals("user_rank"),
            xbins={'start':0.5, 'size':1, 'end':10.5},
            ybins={'start':0.5, 'size':1, 'end':10.5},
            texttemplate="%{z}",
            colorscale='YlGnBu',
            ),1,1)
        fig.add_trace(go.Histogram2d(
            x=vals("user_jobs"), y=vals("user_rank"),
            z=vals("job_dt"), histfunc="avg",
            xbins={'start':0.5, 'size':1, 'end':10.5},
            ybins={'start':0.5, 'size':1, 'end':10.5},
            texttemplate="%{z}",
            colorscale='YlGnBu',
            ),1,2)
        #for col in [1,2]:
        fig.update_xaxes(title_text="active jobs")
        fig.update_yaxes(title_text="starting job rank")
        #                     row=1, col=col)
        fig.write_html("urank_jobs.html",
                       full_html=False,
                       include_plotlyjs='cdn')
        #fig.show()

if __name__=="__main__":
    findings()

