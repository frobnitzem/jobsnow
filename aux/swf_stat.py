import numpy as np
import plotly.express as px
import plotly.graph_objects as go

from datetime import datetime, timedelta

from parse_swf import parse_swf, status_codes

def main(argv):
    assert len(argv) == 3, f"Usage: {argv[0]} <data.swf> <start time>"
    df = parse_swf(argv[1])
    start_time = argv[2]

    save_nodes(df)

    #start_time = "Sat Dec 31 00:20:56 2022" # summit
    #start_time = "Mon Jan  2 12:20:53 2023" # frontier
    t0 = datetime.strptime(start_time, "%a %b %d %H:%M:%S %Y")
    save_clock(df, t0)
    save_weekday(df, t0)
    plot_survival(df)
    plot_sizes(df)

def save_nodes(df):
    max_nodes = df["nprocs"].max()
    with open("nnodes.dat", "w", encoding="utf-8") as f:
        count, _ = np.histogram(df["nprocs"].values,
                                bins=np.arange(max_nodes+1)+0.5)
        print(count.sum(), len(df))
        for n, c in enumerate(count):
            f.write(f"{n+1} {c}\n")

def save_clock(df, time0):
    def to_hour(dt):
        t = time0 + timedelta(seconds=dt)
        return t.hour + (t.minute+t.second/60.0)/60.0

    df["hour"] = df["submit_time"].apply(to_hour)
    with open("clock.dat", "w", encoding="utf-8") as f:
        bins = np.arange(24*4+1)/4.0
        count, _ = np.histogram(df["hour"].values, bins=bins)
        print(count.sum(), len(df))
        for b, c in zip(bins, count):
            f.write(f"{b:.2f} {c}\n")

def save_weekday(df, time0):
    # 0 = Sunday, ..., 6 = Saturday
    def to_day(dt):
        t = time0 + timedelta(seconds=dt)
        return int(f"{t:%w}")

    df["day"] = df["submit_time"].apply(to_day)
    with open("weekday.dat", "w", encoding="utf-8") as f:
        bins = np.arange(8)-0.5
        count, _ = np.histogram(df["day"].values, bins=bins)
        print(count.sum(), len(df))
        for n, c in enumerate(count):
            f.write(f"{n} {c}\n")

def plot_sizes(df):
    fig = go.Figure()
    max_nodes = df["nprocs"].max()
    xbin = go.histogram.XBins(start=0, end=max_nodes, size=10)

    nprocs = df["nprocs"].values.copy()
    nprocs.sort()
    count = np.arange(len(nprocs),0,-1)/len(nprocs)
    fig.add_trace(go.Scatter(x=nprocs, y=count,
                             mode="lines", name="nodes"))
    #fig.add_trace(go.Histogram(
    #    x=df["nprocs"], xbins=xbin, name="nodes")
    #)
    # fractal structure hypothesis:
    # P(X) = \sum_n C_n P(X = x*n)
    #fig.add_trace(go.Histogram(
    #    x=2*df["nprocs"], xbins=xbin, name="nodes*2")
    #)
    #fig.add_trace(go.Histogram(
    #    x=3*df["nprocs"], xbins=xbin, name="nodes*3")
    #)
    fig.update_xaxes(type="log")
    fig.update_yaxes(type="log")
    fig.update_layout(
        xaxis_title="nodes",
        yaxis_title="P(request >= nodes)",
    )


    fig.write_html("sizes.html", full_html=False, include_plotlyjs="cdn")
    fig.show()

def to_status_str(n):
    if n == -1:
        return ""
    if n == 0:
        return "failed"
    if n == 1:
        return "success"
    if n == 4:
        return "killed"
    return "canceled"

def plot_survival(df):
    df["status"] = df["status"].apply(to_status_str)
    done = df.loc[df["run_dt"] > 0]
    fig = go.Figure()
    for fate in ["success", "failed", "killed", "canceled"]:
        survival = done.loc[done["status"] == fate, "run_dt"].values.astype(float).copy()
        if len(survival) == 0:
            continue
        survival.sort()
        survival /= 60.0**2 # hours
        print(f"  {fate}: N = {len(survival)} T = {survival.sum()}")
        count = np.arange(len(survival),0,-1)/len(survival)
        fig.add_trace(go.Scatter(x=survival, y=count,
                                 mode="lines", name=fate))
    fig.update_xaxes(type="log")
    fig.update_yaxes(type="log")
    fig.update_layout(
        xaxis_title="time / hr",
        yaxis_title="P(done >= time)",
    )
    fig.write_html("survival.html", full_html=False, include_plotlyjs="cdn")
    fig.show()

if __name__=="__main__":
    import sys
    main(sys.argv)
