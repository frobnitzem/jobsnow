import pandas as pd

status_codes = {
    -1: "unknown",
    0: "failed",
    1: "success",
    2: "partial",
    3: "partial_complete",
    4: "partial_failed",
    5: "canceled"
}

def main(argv):
    assert len(argv) == 3, f"Usage: {argv[0]} <file.swf> <out.pq>"
    fname = argv[1]
    out = argv[2]
    df = parse_swf(fname)

    #for i in range(3):
    #    print(df.iloc[i])
    print(f"Read {len(df)} records.")
    print(df.head())
    df.to_parquet(out)

def parse_swf(fname):
    # note: all time units are seconds
    record_types = \
      [ ("jobid", int)
      , ("submit_time", int)
      , ("queue_dt", int)
      , ("run_dt", int)
      , ("nprocs_used", int)
      , ("cpu_time", int) # averaged over all CPUs (so < run_dt)
      , ("mem_used", int) # kibyte
      , ("nprocs", int)
      , ("time_request", int)
      , ("mem_request", int) # kibyte
      , ("status", int) # completion status
      , ("uid", int)
      , ("gid", int)
      , ("program", int)
      , ("queue", int)
      , ("partition", int)
      , ("prev_jobid", int)
      , ("prev_dt", int) # think time from preceding job
      ]
    records = {}
    for name, rec_type in record_types:
        records[name] = []
    
    line_no = 0
    for line in open(fname, encoding="utf-8").readlines():
        line_no += 1
        line1 = line.split(';')[0]
        tok = line1.split()
        if len(tok) == 0:
            continue
        if len(tok) < len(record_types):
            print(f"Syntax error on line {line_no}: {line}")
        for i, ((name, rt), s) in enumerate(zip(record_types, tok)):
            records[name].append( rt(s) )

    return pd.DataFrame(records, columns=[name for (name,rt) in record_types])

if __name__=="__main__":
    import sys
    main(sys.argv)
