# Generates per-user data
# for making scatter-plots.
from findings import read_data

import pandas as pd
import numpy as np
import plotly.express as px
from plotly.subplots import make_subplots

def add_plot(fig, row, col, plot):
    for trace in plot["data"]:
        fig.append_trace(trace, row=row, col=col)

def mean(x, wt):
    s = np.sum(wt)
    return np.dot(x, wt)/(s + (s == 0.0))

def show():
    df = read_data()

    user_active  = df.loc[df.user_jobs > 0]
    user_waiting = user_active.loc[user_active.user_run_jobs==0]

    # gather a battery of per-user averages
    user_time = user_active.groupby("user").agg({"user_dt":"sum"})
    print(user_time)
    #low = set(user_time.loc[user_time.user_dt < 1/60.0].index)
    #print(low)
    #user_active = user_active.drop(
    #                    user_active.user.isin(low).index )

    user_active["norm_jobs"] = user_active["user_run_jobs"] \
                             + (user_active["user_run_jobs"]==0)
    stats = user_active.groupby("user").apply(
        lambda x: pd.Series(
            { "pend_jobs": mean(x.user_pend_jobs, x.user_dt),
              "run_jobs":  mean(x.user_run_jobs, x.user_dt),
              "pend_nodes": mean(x.user_pend_nodes, x.user_dt*x.user_pend_jobs),
              "run_nodes": mean(x.user_run_nodes/(x.user_run_jobs+(x.user_run_jobs==0)), x.user_dt*x.user_run_jobs),
              "active_wait": np.dot((x.user_run_jobs==0), x.user_dt),
              "active_total": np.sum(x.user_dt),
              "avg_wait0": mean(
                  x.loc[x.event=="running","job_dt"],
                  x.loc[x.event=="running","user_rank"]==0),
              "avg_wait": np.average(x.loc[x.event=="running","job_dt"]),
           })
    )
    stats["jobs"] = stats.pend_jobs + stats.run_jobs
    stats["pct_wait"] = stats["active_wait"]/stats["active_total"]
    # remove user names
    stats = stats.reset_index().drop("user",axis=1)
    print(stats)

    #wait  = user_waiting.groupby("user").agg({"user_dt": "sum"}))
    #total = user_active.groupby("user").agg({"user_dt": "sum"}))
    #user_active["waiting"] = user_active.user_run_jobs==0
    #user_active["user_dt"] = np.minimum(user_active["user_dt"], 5)

    # job sizes
    #fig = px.violin(
    #        df.loc[df["event"]=="running"],
    #        y="nodes",
    #        x="bin",
    #        color="is_leader",
    #        hover_data="job_count",
    #        box=True)
    #fig.show()

    fig = make_subplots(rows=2, cols=2)
    add_plot(fig, 1, 1,
             px.scatter(stats, x="avg_wait", y="pct_wait",
                        size="run_nodes",
                        color="pct_wait",
                        hover_data=stats.columns))
    fig.update_xaxes(title_text="avg_wait / hr", row=1, col=1)
    fig.update_yaxes(title_text="pct_wait", row=1, col=1)

    #add_plot( px.scatter(stats, x="pct_wait", y="avg_wait",
    #                        hover_data=stats.columns)

    add_plot(fig, 1, 2,
             px.scatter(stats, x="avg_wait", y="avg_wait0",
                        size="run_nodes",
                        color="pct_wait",
                        hover_data=stats.columns))
    fig.update_xaxes(title_text="avg_wait / hr", row=1,col=2)
    fig.update_yaxes(title_text="avg_wait0 / hr", row=1,col=2)

    add_plot(fig, 2, 1,
             px.scatter(stats, x="pend_jobs", y="avg_wait",
                        size="run_nodes",
                        color="pct_wait",
                        hover_data=stats.columns))
    fig.update_xaxes(title_text="pend_jobs", row=2,col=1)
    fig.update_yaxes(title_text="avg_wait / hr", row=2,col=1)

    add_plot(fig, 2, 2,
             px.scatter(stats, x="run_nodes", y="avg_wait0",
                        size="jobs",
                        color="pct_wait",
                        hover_data=stats.columns))
    fig.update_xaxes(title_text="run_nodes", row=2,col=2)
    fig.update_yaxes(title_text="avg_wait0 / hr", row=2,col=2)
    if True:
        fig.write_html("user_avg.html",
                       full_html=False,
                       include_plotlyjs='cdn')
    #fig.show()

if __name__=="__main__":
    show()
