# Job Queue Simulator

The simulator generates `Event`-s that correspond
roughly to the user and job behavior observed on an
HPC system.

The following types of events are modeled:

1. Job-level events

  - Completion of an existing job: `Event("done")` (potentially in killed/failed/completed status)

  - Canceling an existing job: `Event("done")` (results in "canceled" status)
    Although this is done by a user, it is modeled as a
    per-job rate process.

  - Note: For practical reasons, each job has a known
    completion time and status when it is created.
    This should be treated as secret info, since only
    the event system should know about it, not the
    scheduler.

2. Per-user events

  - New job submission: `Event("queued")`

3. System-level events

  - New users enter active use on the machine.

  - Note: A user exits active use automatically when
    that user has no more jobs in the queue.

  - Add/remove nodes from the queue

Most events are modeled as exponential rate
processes, under some pre-conditions for occuring.

This means we need several rate constants:

- rate constant for user submitting a new job

- rate constant for a pending job to be canceled

- rate constant for a running job to complete

## References

On drawing from a set of exponential waiting distributions:

- Arthur F. Voter, [Introduction to the Kinetic Monte Carlo
Method](http://helper.ipam.ucla.edu/publications/matut/matut_5898_preprint.pdf), Los Alamos National Lab Technical Report.

- A.B. Bortz, M.H. Kalos, and J.L. Lebowitz, J. Comp. Phys. 17, 10 (1975).
