# User counter tracking

User counters typically represent time-integrals (or averages)
of user activity over some past time period.

This project tracks "waiting" time, measured as an exponential
moving average of "1" if the user is waiting for a job to run,
and "0" otherwise. The `State.calc_y` function implements this.

It also tracks user node usage, measured as the exponential
moving average of "n(t)", the number of nodes running the user's
jobs at time t. The `State.calc_node_hrs` function implements this.

Both make use of a transformation of the running integral
into a scaling of the past plus interval update:

$$
h(t) = \int_{-\infty}^t e^{-(t-t')/T} n(t') dt'
$$

$$
\begin{aligned}
h(t) &= \int_{-\infty}^x e^{-(t-t')/T} n(t') dt'
      + \int_x^t e^{-(t-t')/T} n(t') dt' \\
     &= h(x) e^{-(t-x)/T} + n(x) \int_0^{t-x} e^{-(t-s-x)/T} dt' \\
     &= e^{-(t-x)/T} ( h(x) + n(x) \int_0^{t-x} e^{s/T} dt' ) \\
     &= e^{-(t-x)/T} ( h(x) + T n(x) (e^{(t-x)/T}-1) ) \\
     &= T n(x) + e^{-(t-x)/T} ( h(x) - T n(x) )
\end{aligned}
$$

Note, however, that `calc_y` uses `T = const_b`, and
is scaled down by `1/const_b`, so that it lies in
the interval `[0,1]`. 
