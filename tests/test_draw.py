import numpy as np

from jobsnow.state import State
from jobsnow.rates import draw_action
from jobsnow.draw_event import draw_new_job
from jobsnow.draw_job import JobGenerator

def test_simulate():
    s = State(0.0)
    rng = np.random.default_rng() # an np.random.Generator
    max_nodes = 64
    proj = "123"

    time = 0.0
    for i in range(4):
        dt, uid = draw_action(rng, [s.time_const()])
        assert uid == 0
        time += dt
        e = draw_new_job(rng, uid, proj, time, max_nodes)
        s.process_event(e)
        print(e)
        print(s)

def test_simulate2():
    nuser = 50
    users = [State(0.0) for i in range(nuser)]

    rng = np.random.default_rng() # an np.random.Generator

    max_nodes = 4600
    proj = "123"
    time = 0.0
    for i in range(4):
        dt, uid = draw_action(rng, [u.time_const() for u in users])
        time += dt
        e = draw_new_job(rng, uid, proj, time, max_nodes)
        users[uid].process_event(e)

        print(uid, e)
        print(users[uid])

def test_gen():
    rng = np.random.default_rng() # an np.random.Generator

    for system in ["summit", "frontier"]:
        G = JobGenerator(system)
        for i in range(100):
            ans = G.sample(rng, 3.0)
            assert ans > 0
