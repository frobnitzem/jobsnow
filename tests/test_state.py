import io

from jobsnow.state import State
from jobsnow.workload import EventStream

from test_workload import swf_file

def test_state():
    f = io.StringIO(swf_file)
    es = EventStream(f)

    a = State(es.start_time)
    t = 0
    for event in es:
        a.process_event(event)
        t = event.time

    assert abs(a.last_update - t) < 1e-8
    assert a.calc_y() > 0.0
    assert len(a.pend_jobs) == 0
    assert len(a.jobs) == 0
