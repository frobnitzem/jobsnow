from jobsnow.expavg import ExpAverage

def test_expavg():
    at = ExpAverage(10.0)
    bt = ExpAverage(20.0)

    a1 = at.calc(1.0, 3.0)
    b1 = bt.calc(1.0, 3.0)

    assert a1 < b1 # exp(-t/10) < exp(-t/20)
    a2 = at.append(1.0, 3.0)
    b2 = bt.append(1.0, 3.0)

    assert abs(a1-a2) < 1e-8
    assert abs(b1-b2) < 1e-8

    a3 = at.calc(10.0, 0.0)
    b3 = bt.calc(10.0, 0.0)

    assert abs(a1-a3) < 1e-8
    assert abs(b1-b3) < 1e-8
