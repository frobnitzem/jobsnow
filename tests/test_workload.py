from pathlib import Path

from jobsnow.workload import EventStream

swf_file = """
;  --- SWF file ---
;       Version: 2.2
;      Computer: Test
;
;  Installation: None
;   Acknowledge: David M. Rogers, ORNL
;   Information: https://gitlab.com/frobnitzem/jobsnow
;
;       MaxJobs: 5
;    MaxRecords: 5
;
; UnixStartTime: 1703080800
;
; TimeZoneString: US/Eastern
; StartTime: Wed Dec 20 09:00:00 EST 2023
; EndTime:   Thu Dec 21 17:00:00 EST 2023
;
; MaxNodes: 10
; MaxProcs: 160
;
; MaxQueues: 2
;    Queues: Queue 1 is default, Queue 2 is debug
;     Queue: 1 - compute - 10 nodes
;     Queue: 2 - debug - all nodes available, limited run-time
1   0 20  33 2  30 4000 2  500 -1 0 1 1 -1 2 1 -1 -1
2  10 40  -1 4  -1 6000 4  500 -1 6 1 1 -1 2 1 -1 -1
3 100 15  61 2  60 4000 2  500 -1 1 1 1 -1 1 1 -1 -1
4 110 20  65 4  60 6000 4  500 -1 1 1 1 -1 1 1 -1 -1
5 200 10 122 4 120 8000 4 1000 -1 1 1 1 -1 1 1 -1 -1
"""

def test_stream(tmp_path : Path):
    swf = tmp_path / "test.swf"
    swf.write_text(swf_file, encoding="utf-8")

    n = 0
    with swf.open() as f:
        es = EventStream(f)
        for event in es:
            print(event)
            n += 1
    assert n == 14
