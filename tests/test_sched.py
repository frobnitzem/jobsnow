
import numpy as np

from jobsnow.events import JobEvent
from jobsnow.simulate import SimulatedSystem

def test_user():
    rng = np.random.default_rng()
    S = SimulatedSystem(rng, 0.0, max_nodes=4000)
    skipped_jobs = set()
    for i in range(2):
        assert len(S.events) > 0
        t = S.next_time()
        assert t is not None
        for e in S.advance(rng, (t-S.time)*5):
            assert S.time == e.time
            if e.name == JobEvent.queued:
                skipped_jobs.add( e.job.jobid )
            elif e.name == JobEvent.canceled:
                if e.job.running is None:
                    skipped_jobs.remove( e.job.jobid )

            utilized = sum([job.nodes for job in S.list_running()])
            assert utilized == S.max_nodes - S.avail_nodes

    pend = S.list_pending()
    assert len(pend) == len(skipped_jobs)
    for p in pend:
        assert p.jobid in skipped_jobs

def test_sim():
    rng = np.random.default_rng()
    S = SimulatedSystem(rng, 0.0, max_nodes=4000)
    skipped_jobs = 0
    for i in range(10):
        assert len(S.events) > 0
        t = S.next_time()
        assert t is not None
        for e in S.advance(rng, (t-S.time)*2):
            # Opportunistic policy - never revisit waiting
            # jobs.
            print(e)
            if e.name == JobEvent.queued:
                if S.avail_nodes >= e.job.nodes:
                    print("starting")
                    n = len(S.events)
                    S.start_job(rng, e.job)
                    assert len(S.events) == n+1
                else:
                    skipped_jobs += 1
            utilized = sum([job.nodes for job in S.list_running()])
            assert utilized == S.max_nodes - S.avail_nodes

    pend = S.list_pending()
    assert len(pend) == skipped_jobs
    print( S.show_user_stats() )

    print( S.qstat() )
