import io

from jobsnow.state import State
from jobsnow.workload import EventStream
from jobsnow.logger import LogSystem

from test_workload import swf_file

def test_logger():
    f = io.StringIO(swf_file)
    es = EventStream(f)
    print(es.info)

    t0 = es.info["UnixStartTime"]
    L = LogSystem(t0, es.info["MaxProcs"])

    t = 0
    for event in es:
        L.add(event)
        t = event.time

    assert abs(L.sys.last_update - t) < 1e-8
    for v in L.events.values():
        assert len(v) == 14
