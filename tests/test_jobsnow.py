from pathlib import Path

from typer.testing import CliRunner

from jobsnow.jobsnow import app

from test_workload import swf_file

runner = CliRunner()

def test_analyze(tmp_path : Path):
    swf = tmp_path / "test.swf"
    swf.write_text(swf_file, encoding="utf-8")
    result = runner.invoke(app, ["analyze", str(swf)])
    print(result.stdout)
