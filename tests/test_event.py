import pytest

from jobsnow.events import Event, Job, JobEvent

def test_job():
    job = Job(jobid = 123, nodes = 10, user = 0, proj = "ok", queue = "batch")
    ev = Event(JobEvent.queued, time = 1.1, job=job)
    job.process_event(ev)
    with pytest.raises(ValueError):
        print(ev.to_swf(0, 0.0))

    ev = Event(JobEvent.canceled, time = 1.2, job=job)
    job.process_event(ev)

    print(ev.to_swf(0, 0.0))

    job2 = Job(jobid = 133, nodes = 20, user = 0, proj = "ok", queue = "batch")
    job2.process_event(
        Event(JobEvent.queued, time = 2.1, job=job2))
    job2.process_event(
        Event(JobEvent.running, time = 3.2, job=job2))
    ev = Event(JobEvent.success, time = 4.2, job=job2)
    job2.process_event(ev)
    print(ev.to_swf(1, 0.0))
