from typing import Optional, Tuple
import numpy as np

from .events import Job, Event, JobEvent
from .state import State

def softmax(x):
    y = np.exp(x - x.max())
    return y / y.sum()

class System:
    time : float
    max_nodes : int
    avail_nodes : int
    users  : dict[int,State]
    projs  : dict[str,State]
    jobs   : dict[int,Job]

    def __init__(self, time : float, max_nodes : int) -> None:
        self.time = time
        self.max_nodes = max_nodes
        self.avail_nodes = max_nodes

        self.users = {}
        self.projs = {}
        self.jobs = {}

    def process_event(self,
                      event : Event,
                      check : bool = True
                     ) -> Optional[Tuple[int,float,int,float]]:
        """ Update the state of self.time, self.avail_nodes,
            self.projs[job.proj], self.users[job.user],
            and self.jobs

            to reflect this event has occurred.
        """
        job = event.job
        self.time = event.time

        if event.name == JobEvent.queued:
            if job.proj not in self.projs:
                self.projs[job.proj] = State()
            if job.user not in self.users:
                self.users[job.user] = State()
            assert job.jobid not in self.jobs, "Already queued."
            job.queued = self.time
            self.jobs[job.jobid] = job
        elif event.name == JobEvent.running:
            if check:
                assert self.avail_nodes >= job.nodes
            job.running = self.time
            self.avail_nodes -= job.nodes
        elif event.name.is_final():
            # Ignore final events on non-existing jobs.
            if job.jobid not in self.jobs:
                return None

        # process event internally
        rp, dtp = self.projs[job.proj] . process_event(event)
        ru, dtu = self.users[job.user] . process_event(event)

        if event.name.is_final():
            job.done = self.time
            self.jobs.pop(job.jobid)
            if job.running is not None:
                self.avail_nodes += job.nodes

        if dtp is None:
            dtp = 0.0
        if dtu is None:
            dtu = 0.0
        return rp, dtp, ru, dtu

    def active_users(self):
        n = 0
        for u in self.users.values():
            n += len(u.jobs) > 0
        return n

    def active_projs(self):
        n = 0
        for p in self.projs.values():
            n += len(p.jobs) > 0
        return n

    def list_pending(self):
        """ List all the pending jobs at `self.time`.
        
            Each job returned has extra information
            provided by calc_user_stats.
            Note that for "non-leading" jobs, both x,y are 0.
            (TODO: consider saving these as x1,y1 in case
             they are useful / relevant).
        """
        order, y, hrs = calc_user_stats(self.users, self.time)
        x = softmax(y)
        total = hrs.sum() + (hrs.sum() == 0.0)

        jobs = []
        for uid, u in self.users.items():
            if len(u.pend_jobs) == 0:
                continue

            leader = u.lead_jobid()
            ujobs = [u.jobs[p] for p in u.pend_jobs]
            # whether the user is waiting for a leading job to run
            #user_waiting = min(u.pend_jobs) == leader
            i = order[uid]
            for j in ujobs:
                mask = int(j.jobid == leader)
                j.x = x[i]*mask
                j.y = y[i]*mask
                j.hrs = hrs[i]
                j.frac = hrs[i]/total

            jobs.extend(ujobs)

        return jobs

    def list_running(self):
        """ List all running jobs at `self.time`.
        """
        order, y, hrs = calc_user_stats(self.users, self.time)
        x = softmax(y)
        total = hrs.sum() + (hrs.sum() == 0.0)

        jobs = []
        for uid, u in self.users.items():
            if len(u.jobs) == 0:
                continue

            leader = min(u.jobs.keys())
            ujobs = [u.jobs[jobid] for jobid in \
                        set(u.jobs.keys()) - set(u.pend_jobs)]
            i = order[uid]
            for j in ujobs:
                mask = int(j.jobid == leader)
                j.x = x[i]*mask
                j.y = y[i]*mask
                j.hrs = hrs[i]
                j.frac = hrs[i]/total

            jobs.extend(ujobs)

        return jobs

    def qstat(self) -> str:
        hdr = "| UID | JobID | Nodes | UserPrio | UserWait | UserUsage |"
        sep = "".join("|" if c == "|" else "-" for c in hdr)

        s = ["# Running Jobs\n", hdr, sep]
        for job in self.list_running():
            s.append( f"| {job.user:3d} | {job.jobid:5d} | {job.nodes:5d} | {job.x:8f} | {job.y:8f} | {job.hrs:9f} |" )

        s.extend(["\n# Pending Jobs\n", hdr, sep])
        for job in self.list_pending():
            s.append( f"| {job.user:3d} | {job.jobid:5d} | {job.nodes:5d} | {job.x:8f} | {job.y:8f} | {job.hrs:9f} |" )

        return "\n".join(s)

    def show_user_stats(self) -> str:
        order, y, hrs = calc_user_stats(self.users, self.time)
        show = lambda uid, i: f"{uid:3d} {y[i]} {hrs[i]}"
        return "\n".join(show(uid, i) \
                         for uid,i in order.items())

from typing import TypeVar

T = TypeVar('T')
def calc_user_stats(users : dict[T,State], time : float
        ) -> Tuple[dict[T,int], np.ndarray, np.ndarray]:
    """ Calculate some arrays, with entries for each user,

        - order: mapping from uid to sequential array index
        - y: waiting length
        - hrs: number of node-hours consumed by this user
    """

    y = np.zeros(len(users))
    hrs = np.zeros(len(users))
    order = dict( (uid,i) for i, uid in \
                          enumerate(users.keys()) )
    for uid, u in users.items():
        i = order[uid]
        if u.last_update is None:
            hrs[i] = 0.0
            y[i] = 0.0
            continue
        dt = time - u.last_update
        if dt < 0.0:
            print(f"Negative dt = {time} - {u.last_update} !!!")
            dt = 0.0

        hrs[i] = u.calc_node_hrs(dt)
        y[i]   = u.calc_y(dt)
    
    return order, y, hrs
