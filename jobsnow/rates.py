from typing import Optional, Any, Tuple, Sequence
import bisect
from math import log

import numpy as np

Generator = np.random.Generator

def draw_sorted(rng : Generator, sump : np.ndarray) -> int:
    """ Draw an index [0, len(sump)) with probabilities
        given by (sump[i+1]-sump[i]) / sump[-1].
    """
    return bisect.bisect_left(sump, rng.random()*sump[-1])

# time to next event
def draw_timedelta(rng, k : float) -> float:
    return -log(1.0 - rng.random()) / k

def draw_action(rng    : Generator,
                rates  : Sequence[float],
               ) -> Tuple[float, int]:
    """ Draw a random event time and originator
        out of the possibilities: any actor, or "other".

        If other is None, then a State is always returned.
        Otherwise other is a rate constant for returning
        the "None" event.
    """
    k = np.array(rates) . cumsum()
    assert len(k) > 0
    dt = draw_timedelta(rng, k[-1])
    i = draw_sorted(rng, k)
    return (dt, i)

