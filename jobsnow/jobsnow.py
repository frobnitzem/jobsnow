from typing import Optional, List, get_args
from pathlib import Path
from typing_extensions import Annotated
from enum import Enum
from time import time as get_time
import logging
_logger = logging.getLogger(__name__)

import numpy as np
import typer

from .workload import EventStream
from .state import State
from .analyze import event_stats
from .draw_job import JobGenerator
from .logger import LogSystem

def setup_logging(v, vv):
    if vv:
        logging.basicConfig(level=logging.DEBUG)
    elif v:
        logging.basicConfig(level=logging.INFO)

app = typer.Typer()

V1 = Annotated[bool, typer.Option("-v", help="show info-level logs")]
V2 = Annotated[bool, typer.Option("-vv", help="show debug-level logs")]

def to_str(x):
    if isinstance(x, Enum):
        return x.value
    return str(x)

@app.command()
def analyze(swf : Path,
            output : Path,
            v : V1 = False, vv : V2 = False):
    """
    Analyze a workload file in SWF format.

    Output the results in the directory `output`
    """
    setup_logging(v, vv)
    output.mkdir()
    with swf.open() as f:
        es = EventStream(f)
        stats = event_stats(es)

    for k, val in stats.items():
        with open(output / k, "w", encoding="utf-8") as f:
            for x in val:
                if isinstance(x, (int,float)):
                    f.write(f"{x}\n")
                else:
                    s = " ".join(map(to_str, x))
                    f.write( s + "\n" )

@app.command()
def events(swf : Path,
           output : Path,
           v : V1 = False, vv : V2 = False):
    """
    Output the event sequence from a workload file in SWF format.

    Output the results in the directory `output`
    """
    setup_logging(v, vv)

    with swf.open() as f:
        es = EventStream(f)
        t0 = es.info["UnixStartTime"]
        L = LogSystem(t0, es.info["MaxProcs"])
        for ev in es:
            L.add(ev)

    L.write(output)

@app.command()
def simulate(model : Path,
             v : V1 = False, vv : V2 = False):
    """
    Simulate a workload using the given model.
    """
    setup_logging(v, vv)
    t0 = int(get_time())
    rng = np.random.default_rng() # an np.random.Generator

    time = t0
    for system in ["summit", "frontier"]:
        G = JobGenerator(model)
        for i in range(100):
            ans = G.sample(rng, time)
            assert ans > 0
            time += ans
