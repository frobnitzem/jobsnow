import numpy as np

Generator = np.random.Generator

from .events import Event, JobEvent, new_job
from .draw_job import JobGenerator

_gen = JobGenerator("summit")
def draw_new_job(rng : Generator,
                 uid : int,
                 proj : str,
                 time : float,
                 max_nodes : int) -> Event:
    # FIXME: replace max_nodes with the JobGenerator itself
    queue = "batch"
    job = new_job(min(_gen.sample(rng, time), max_nodes),
                  uid, proj, queue)
    return Event(JobEvent.queued, time, job)
