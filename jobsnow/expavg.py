import numpy as np

class ExpAverage:
    """ Accumulate an expontential moving average:

        \bar x(t) = \int_{-\infty}^t e^{(t'-t)/T} x(t') dt'

        By time intervals, dt, over which x(t')
        remains constant.
    """
    def __init__(self, T : float, x0 : float = 0.0) -> None:
        self.T = T
        self.x = x0

    def calc(self, x1, dt):
        # Return the value of \bar x(t) after dt
        # but do not update self.x
        if dt < 0.0:
            return self.x
        return self.T*x1 \
               + np.exp(-dt/self.T)*(self.x - self.T*x1)

    def append(self, x1, dt):
        # Return the value of \bar x(t) after dt
        # and update self.x
        self.x = self.calc(x1, dt)
        return self.x
