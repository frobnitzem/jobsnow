from typing import Optional
from enum import Enum
from dataclasses import dataclass

global_jobid = 1

class JobEvent(str, Enum):
    queued   = "queued"
    running  = "running"
    success  = "success"
    failed   = "failed"
    canceled = "canceled"
    killed   = "killed"

    def is_final(self) -> bool:
        return self.value in ["success", "canceled", "failed", "killed"]

    def num(self):
        # Give a numerical order so that two events occuring
        # at the same time for the same job can be sequenced.
        if self.value == "queued":
            return 1
        elif self.value == "running":
            return 2
        elif self.value == "success":
            return 3
        elif self.value == "failed":
            return 4
        elif self.value == "canceled":
            return 5
        elif self.value == "killed":
            return 6
        return 0

    def __lt__(a, b):
        return a.num() < b.num()
    def __le__(a, b):
        return a.num() <= b.num()
    def __gt__(a, b):
        return a.num() > b.num()
    def __ge__(a, b):
        return a.num() >= b.num()

@dataclass
class Job:
    jobid : int
    nodes : int
    user  : int
    proj  : str
    queue : str
    queued : Optional[float] = None
    running : Optional[float] = None
    done    : Optional[float] = None

    def last_update(self) -> Optional[float]:
        if self.done is not None:
            return self.done
        if self.running is not None:
            return self.running
        return self.queued

    def process_event(self, ev : "Event"):
        if ev.name == JobEvent.queued:
            self.queued = ev.time
        elif ev.name == JobEvent.running:
            self.running = ev.time
        else:
            self.done = ev.time

    def __lt__(a, b):
        return a.jobid < b.jobid
    def __le__(a, b):
        return a.jobid <= b.jobid
    def __gt__(a, b):
        return a.jobid > b.jobid
    def __ge__(a, b):
        return a.jobid >= b.jobid
    def __eq__(a, b):
        return a.jobid == b.jobid

class Numbering:
    db : dict[str,int]
    def __init__(self) -> None:
        self.db = {}
    def __call__(self, name : str) -> int:
        try:
            n = self.db[name]
        except KeyError:
            n = len(self.db)
            self.db[name] = n
        return n
    def show(self, name) -> str:
        records = "\n".join(f";     {name}: {v} - {k}" \
                            for k, v in self.db.items())
        return f"""; Max{name}s: {len(self.db)}
;    {name}s: {" ".join(self.db.keys())}\n""" + records

map_proj = Numbering()
map_queue = Numbering()

class Event:
    name : JobEvent
    time : float
    job  : Job
    def __init__(self, name : JobEvent,
                 time : float,
                 job : Job,
                ) -> None:

        self.name = name
        self.time = time
        self.job = job

    def __lt__(a, b):
        if a.time == b.time:
            if a.job == b.job:
                return a.name < b.name
            return a.job < b.job
        return a.time < b.time
    def __gt__(a, b):
        return b < a
    def __le__(a, b):
        return not (b < a)
    def __ge__(a, b):
        return not (a < b)
    def __eq__(a, b):
        return a.time == b.time and a.job == b.job and a.name == b.name

    def __repr__(self):
        return f"Event({self.name}, {self.time}, {self.job})"

    def to_swf(self, num : int, t0 : float) -> str:
        fate = 0
        if self.name.value == JobEvent.success:
            fate = 1
        elif self.name.value == JobEvent.failed:
            fate = 0
        elif self.name.value == JobEvent.canceled:
            fate = 5
        elif self.name.value == JobEvent.killed:
            fate = 6
        else:
            raise ValueError(f"invalid to_swf state: {self.name}")
        job = self.job
        q = -1.0
        wait = -1.0
        run = -1.0
        if job.queued is not None:
            q = job.queued - t0
            if job.running is not None:
                wait = job.running - job.queued
                if job.done is not None:
                    run = job.done - job.running
            elif job.done is not None: # canceled
                fate = 6
                wait = job.done - job.queued
        else:
            raise ValueError(f"Job has no queue time: {job}")

        proj = map_proj(job.proj)
        queue = map_queue(job.queue)
        return f"{num} {q} {wait} {run} {job.nodes} {run} -1 {job.nodes} -1 -1 {fate} {job.user} {proj} -1 {queue} -1 -1 -1"

def to_event(info, jobs) -> Event:
    event = JobEvent( info["EVENT"] )
    jobid = int(info["JOBID"])
    time  = float(info["TIME"])
    if event == JobEvent.queued:
        job = Job(jobid, info["NODES"],
                  info["USER"], info["PROJ"],
                  info["QUEUE"])
        return Event(event, time, job)

    job = jobs[jobid]
    return Event(event, time, job)

def infer_events(df) -> list[Event]:
    """ Infer all events that must have occured to
        reach the current state of PEND/RUN jobs.
    """
    events : list[Event] = []
    for _, row in df.loc[df["STAT"]=="PEND"].iterrows():
        # may be incorrect (for jobs prev. suspended)
        j = Job(row.JOBID, row.NODES, row.USER, row.PROJ, row.QUEUE)
        events.append( Event(JobEvent.queued,
                             row["SUBMIT_TIME"],
                             j) )
        j.process_event(events[-1])
    for _, row in df.loc[df["STAT"]=="RUN"].iterrows():
        # may be incorrect (for jobs prev. suspended)
        j = Job(row.JOBID, row.NODES, row.USER, row.PROJ, row.QUEUE)
        events.append( Event(JobEvent.queued,
                             row["SUBMIT_TIME"],
                             j) )
        events.append( Event(JobEvent.running,
                             row["START_TIME"],
                             j) )
        j.process_event(events[-2])
        j.process_event(events[-1])
    events.sort(key=lambda x: x.time)
    return events

def new_job(nodes : int, user : int, proj : str, queue : str) -> Job:
    global global_jobid
    jobid = global_jobid
    global_jobid += 1
    return Job(jobid, nodes, user, proj, queue)
