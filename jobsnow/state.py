from typing import Optional, Any, Tuple
import bisect

from math import exp

from .expavg import ExpAverage

from .events import (
    Event,
    Job,
    JobEvent,
    infer_events
)

k_submit = 1.0       # 1 job/user/hr.

const_b  = 30*24*3600.0   # avg. time to "forget" wait time = 1 month
avg_T    = 6*30*24*3600.0 # moving avg usage = 6 months
interactive_t = 30.0      # 30 seconds ~ interactive timescale

class State:
    """ State for all group levels
 
        TODO: jobs does not need to be cached here,
              since it's stored at the System level.
              However, some system-level functions still
              depend on getting the list of jobs by-user.
    """
    pend_jobs   : list[int] # queued subset of jobids (sorted)
    jobs        : dict[int,Job] # queued / running jobids
    pend_nodes  : int # total nodes requested by all pending jobs
    run_nodes   : int # total nodes used by all running jobs
    last_update : Optional[float] # timestamp of last update
    node_hrs    : ExpAverage # node-hours used
    active_t    : ExpAverage # active time
    wait_t      : ExpAverage # active and waiting time

    # Moving average node-hours requested / used:
    # node_hrs(t) = \int_{-\infty}^t exp(-(t-t')/T) nodes(t') dt'
    #   = T nodes(x..t) + exp(-(t-x)/T) [
    #                       node_hrs(x) - T nodes(x..t) ]

    #def __init__(self, df : Optional[pd.DataFrame] = None
    #            ) -> None:
    def __init__(self, time : Optional[float] = None) -> None:
        self.pend_jobs   = []
        self.jobs        = {}
        self.pend_nodes  = 0
        self.run_nodes   = 0
        self.last_update = time
        self.node_hrs    = ExpAverage(avg_T)
        self.active_t    = ExpAverage(const_b)
        self.wait_t      = ExpAverage(const_b)

        # bootstrapping from a dataframe of events
        #if df is not None:
        #    events = infer_events(df)
        #    for e in events:
        #        self.process_event(e)

    def __str__(self):
        return f"State with {len(self.pend_jobs)} pending out of {len(self.jobs)} jobs. Last update at {self.last_update} . {self.pend_nodes} pending and {self.run_nodes} running nodes. node_hrs = {self.calc_node_hrs()}, y = {self.calc_y()}"

    def calc_node_hrs(self, dt : float = 0.0) -> float:
        """ Project running node_hrs forward by dt,
            given that no changes in jobs occur.
        """
        return self.node_hrs.calc(self.run_nodes, dt)

    def calc_y(self, dt : float = 0.0) -> float:
        """ Project y forward by dt,
            given that no changes in jobs occur.
        """
        # Is the user active?
        active = len(self.jobs) > 0
        # Are all jobs pending?
        pend = active * (len(self.pend_jobs) == len(self.jobs))

        x = self.active_t.calc(active, dt)
        y = self.wait_t.calc(pend, dt)
        return y / (x + interactive_t)

    def tick(self, time : float) -> Optional[float]:
        if self.last_update is None:
            self.last_update = time
            return None
        dt = time - self.last_update
        self.last_update = time
        # FIXME: some of these dt values are O(-1 sec)

        # update moving averages
        # Is the user active?
        active = len(self.jobs) > 0
        # Are all jobs pending?
        pend = active * (len(self.pend_jobs) == len(self.jobs))

        self.node_hrs.append(self.run_nodes, dt)
        self.active_t.append(active, dt)
        self.wait_t.append(pend, dt)
        return dt

    def lead_jobid(self) -> Optional[int]:
        if len(self.jobs) == 0:
            return None
        return min(self.jobs.keys())

    def queued(self, time : float, job : Job) -> int:
        # Update state to reflect this job is now queued.
        jobid = job.jobid
        assert jobid not in self.jobs, "Job already queued."
        # rightmost
        rank = bisect.bisect_right(self.pend_jobs, jobid)
        self.pend_jobs.insert(rank, jobid)
        self.jobs[jobid] = job
        self.pend_nodes += job.nodes
        return rank

    def running(self, time : float, job : Job) -> int:
        # Update state to reflect this job is now running.
        jobid = job.jobid
        rank = bisect.bisect_left(self.pend_jobs, jobid) # leftmost
        assert rank < len(self.pend_jobs) and \
                self.pend_jobs.pop(rank) == jobid, f"Job {jobid} not found!"
        self.pend_nodes -= job.nodes
        self.run_nodes  += job.nodes
        return rank

    def done(self, time : float, job : Job) -> Tuple[int, Job]:
        # Update state to reflect this job is now done.
        jobid = job.jobid
        # pop from left
        rank = bisect.bisect_left(self.pend_jobs, jobid)

        if rank < len(self.pend_jobs) and \
                self.pend_jobs[rank] == jobid: # was pending
            self.pend_jobs.pop(rank)
            self.pend_nodes -= job.nodes
        else:
            self.run_nodes -= job.nodes
        job = self.jobs.pop(jobid)
        return rank, job

    def process_event(self, e : Event,
                      update_job : bool = False) -> Tuple[int,Optional[float]]:
        # Update state to reflect the given event.
        dt = self.tick(e.time)
        if e.name == JobEvent.queued:
            rank = self.queued(e.time, e.job)
        elif e.name == JobEvent.running:
            rank = self.running(e.time, e.job)
        elif e.name.is_final():
            rank, _ = self.done(e.time, e.job)
        else:
            raise KeyError(f"Unknown event type: {e.name}")

        if update_job:
            e.job.process_event(e)
        return rank, dt

    def time_const(self):
        """ Rate constant for an event pertaining to this user.
        """
        return k_submit
