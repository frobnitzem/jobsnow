from typing import Any, Tuple, Optional, Union, List, Dict

from .events import JobEvent, Event, Job
from .workload import EventStream
from .system import System
#from .logger import LogSystem

seconds = 1.0
minutes = 60.0*seconds
hours = 60*minutes
days = 24*hours

# Jobs starting faster than interactive_time
# are considered automated / batch submissions.
interactive_time = 5*seconds
# After max_think_time, consider that the old session
# has ended.
max_think_time = 20*minutes

Response = Tuple[JobEvent,float,float,Optional[float]]

def applyList(m, k):
    ans = []
    for i in m:
        ans.extend( k(i) )
    return ans

class JobArray:
    jobs             : List[Job] # all jobs in this array
    last_start       : float     # last job start

    def __init__(self, ev : Event) -> None:
        assert ev.name == JobEvent.queued, f"Out of order event: {ev}"
        self.jobs = [ ev.job ]
        self.last_start = ev.time

    def append(self, ev : Event) -> bool:
        """ Returns True if the event belongs in the current
            array.  If False is returned, a new array
            should be started with this event.
        """
        assert ev.name == JobEvent.queued
        if ev.time-self.last_start > interactive_time:
            return False

        self.jobs.append( ev.job )
        self.last_start = ev.time
        return True

    def stats(self):
        N = len(self.jobs)
        return N, sum(j.nodes for j in self.jobs)/N

# TODO: make some tests following Feitelson's Fig. 7
class Session:
    start            : float           # Session start time
    last_complete    : Optional[float] # time of last completed job
    last_status      : JobEvent        # status of last completed job
    last_queue       : float           # queue time of last completed job
    last_resp        : float           # response time of last completed job
    arrs             : List[JobArray]  # all job arrays in session
    # final status, response time, and think time for all jobs
    responses        : List[Response]

    def __init__(self, ev : Event):
        assert ev.name == JobEvent.queued, f"Out of order event: {ev}"

        self.start       = ev.time
        self.last_complete = None
        self.arrs        = [ JobArray(ev) ]
        self.responses   = []

    def append(self, ev : Event) -> bool:
        """ Returns True if the event belongs in the current
            session.  If False is returned, a new session
            should be started with this event.
        """

        if ev.name == JobEvent.queued:
            ok = self.arrs[-1].append(ev)
            if ok: # no think time for this job (part of array)
                return True

            self.arrs.append( JobArray(ev) )
            if self.last_complete is not None:
                tt = ev.time - self.last_complete
                assert tt >= 0.0
                if tt > max_think_time:
                    self.responses.append((self.last_status,
                                           self.last_queue,
                                           self.last_resp, None))
                    return self.complete()

                self.responses.append((self.last_status,
                                       self.last_queue,
                                       self.last_resp, tt))
                self.last_complete = None

        elif ev.name.is_final():
            self.last_complete = ev.time
            self.last_status   = ev.name
            j = ev.job
            assert j.done is not None, "Incomplete session"
            assert j.queued is not None, "Incomplete session"
            self.last_resp = j.done - j.queued
            if j.running is not None:
                self.last_queue = j.running - j.queued
            else:
                self.last_queue = self.last_resp

        return True

    def stats(self) -> Tuple[List[Response], List[Tuple[int,int]]]:
        """ Returns a list of status/response time/think time,
            and array size/avg nnodes
        """
        arr_sizes = [arr.stats() for arr in self.arrs]
        return self.responses, arr_sizes

    def complete(self) -> bool:
        """ Do any finalization actions on this
            session and return False.
        """
        return False

def event_stats(es : EventStream) -> Dict[str,Any]:
    """ Return lists of:
         #not yet: session initiation times (relative to last Monday, 0:00)
         - session initiation times
         - response times (turnaround time)
         - think times
         - array sizes (for array jobs)
    """
    #S = System(es.start_time, es.info["MaxProcs"])

    start_times      : List[float] = []
    responses        : List[Response] = []
    array_size       : List[Tuple[int,int]] = []
    #singlet_nodes    : List[int]   = []
    #array_nodes      : List[int]   = []
    sessions         : Dict[int, Session] = {}

    def log_session(S):
        start_times.append(S.start)
        resp, asizes = S.stats()
        responses.extend(resp)
        array_size.extend(asizes)
        #session_lengths.append(len(rt))

    for event in es:
        uid = event.job.user
        if uid not in sessions:
            sessions[uid] = Session(event)
            continue

        if sessions[uid].append(event):
            continue

        # completed session
        S = sessions[uid]
        log_session(S)
        sessions[uid] = Session(event)

    for S in sessions.values():
        log_session(S)

    return { "start": start_times,
             "responses": responses,
             "array_size": array_size,
           }
