from functools import cache
import bisect
import importlib.resources
from datetime import datetime

import numpy as np

@cache
def read_tbl(system, name):
    s = importlib.resources.read_text(__package__+f".systems.{system}",
                                      name, encoding="utf-8")
    ans = []
    for line in s.split("\n"):
        tok = line.split()
        if len(tok) != 2: continue
        ans.append(list(map(float, tok)))
    ans = np.array(ans).T
    # TODO: eliminate zeros to compress
    # w = np.where(ans[1] > 0)
    # prob = np.cumsum(ans[1][w])
    # return prob/prob[-1], ans[0][w]
    #
    prob = np.cumsum(ans[1])
    return prob/prob[-1], ans[0]

class JobGenerator:
    # Currently generates the number of nodes in a job
    # submission event.
    #
    # TODO: expand this generator to also generate
    # - time of next job submission for a given user set (total rate)
    # - number of nodes
    # - fate of job (if completed)
    # - running time until completion
    # - time until canceled
    def __init__(self, system):
        self.pnodes, self.nnodes  = read_tbl(system, "nnodes.dat")
        self.nnodes   = self.nnodes.astype(int)
        #self.clock   = read_tbl(system, "clock.dat")
        self.weekday = read_tbl(system, "weekday.dat")

    def next_session(self, gen, time) -> float:
        """ Generate a waiting time (in seconds) until the
            start of the next user session.
            We assume this follows an exponential distribution
            with time stretching.
        """
        s = gen.exponential()
        dt = 0.0
        while s > 0.0:
            s -= self.weekday(time+dt)
            dt += 1.0
        return dt

    def sample(self, gen, time) -> int:
        d = datetime.fromtimestamp(time)
        weekday = d.weekday() # Monday is 0 and Sunday is 6
        hr = float(d.hour) + float(d.minute)/60.0

        r = gen.random()
        i = bisect.bisect_left(self.pnodes, r)
        return self.nnodes[i]
