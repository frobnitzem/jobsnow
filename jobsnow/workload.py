from typing import Any, get_args, Iterator
from datetime import datetime
from enum import Enum
from heapq import heappush, heappop

from .events import JobEvent, Job, Event

def job_events(n : list[str], start_time : float) -> Iterator[Event]:
    """ Yields up to 3 events per job.
        Non-obvious fields in n are:

        - 5  Average CPU Time Used / float
        - 6  Used Memory -- in kilobytes, avg per processor
        - 7  Requested Number of Processors
        - 8  Requested Time
        - 9  Requested Memory
        - 10 Exit Status
        - 11 User ID
        - 12 Group ID
        - 13 Application Number
        - 14 Queue Number
        - 15 partition number
    """
    procs = int(n[7])
    #assert procs == int(n[4]), "Inconsistent requested vs. allocated processors"
    t0   = float(n[1]) + start_time
    assert float(n[1]) >= 0.0
    wait = float(n[2])
    run  = float(n[3])
    status = int(n[10])

    isneg = lambda x: x in ["-1", "-1.0"]

    assert not isneg(n[1]), "Job must have a submit time."
    running = None
    done = None
    if not isneg(n[2]):
        assert wait >= 0.0
        if isneg(n[3]): # canceled
            status = 5
            done = t0 + wait
        else:
            assert run >= 0.0, str(n)
            running = t0+wait
            done = running+run
    job = Job(int(n[0]), procs, int(n[11]), n[12], n[14], t0, running, done)
    yield Event(JobEvent.queued, t0, job)
    if running:
        yield Event(JobEvent.running, running, job)
    if done:
        if status == 1:
            yield Event(JobEvent.success, done, job)
        elif status == 5:
            yield Event(JobEvent.canceled, done, job)
        elif status == 6:
            yield Event(JobEvent.killed, done, job)
        else:
            yield Event(JobEvent.failed, done, job)

class PreEmption(str,Enum):
    No = "No"
    Yes = "Yes"
    Double = "Double"
    TS = "TS"

header_labels = {
    "Version" : str,
    "Computer" : str,
    "Installation" : str,
    "Acknowledge" : str,
    "Information" : str,
    "Conversion" : str,
    "MaxJobs" : int,
    "MaxRecords" : int,
    "Preemption" : PreEmption,
    "UnixStartTime" : int, # / seconds
    "TimeZoneString" : str,
    "StartTime" : str,
    "EndTime" : str,
    #"StartTime" : datetime, # didn't parse...
    #"EndTime" : datetime,
    #"MaxNodes" : str,
    #"MaxProcs" : str,
    "MaxNodes" : int,
    "MaxProcs" : int,
    "MaxRuntime" : int, # / seconds
    "MaxMemory" : int, # / kilobytes
    "AllowOveruse" : bool,
    "MaxQueues" : int,
    "Queues" : str,
    "Queue" : list[str],
    "MaxPartitions" : int,
    "Partitions" : str,
    "Partition" : list[str],
    "Note" : list[str],
}

def parse_hdr(info : dict, line : str) -> bool:
    """ Parse a header line (top of file, excluding leading ';').
        Stores recognized fields into `info`.

        Returns True if a field was recognized and parsed.
    """
    tok = line.split(":", 1)
    if len(tok) != 2:
        return False
    k = tok[0].strip()
    v = tok[1].strip()
    try:
        t = header_labels[k]
    except KeyError:
        return False
    if isinstance(t("0"), list):
        inner = get_args(t)[0]
        if k not in info:
            info[k] = []
        info[k].append( inner(v) )
    elif issubclass(t, datetime):
        info[k] = datetime.strptime(v, "%c")
        #try:
        #    info[k] = datetime.strptime(v, "%a %b %d %I:%M:%S %p %Z %Y") # 12-hr
        #except ValueError:
        #    info[k] = datetime.strptime(v, "%a %b %d %H:%M:%S %Z %Y") # 24-hr
    else:
        info[k] = t(v) #type: ignore[call-arg]
    return True

class EventStream:
    """ Generate a stream of time-ordered events by iterating through
        the swf log
    """
    def __init__(self, f, chunk=500) -> None:
        self.f = f
        self.chunk = chunk

        self.header = True
        self.info : dict[str,Any] = {}
        # event buffer
        self.ebuf : list[Event] = []
        # lower bound on all future times that may appear in the log
        self.time = 0.0 # relative to start_time
        self.lno = 0
        self.eof = False
        self.start_time = 0.0
        self.prefetch()

    def prefetch(self):
        # Read the next "chunk" lines to refill the event buffer.
        end = self.lno + self.chunk
        while not self.eof and (self.header or self.lno < end):
            self.lno += 1
            line = self.f.readline()
            if not line:
                self.eof = True
                break
            parts = line.split(';', 1)
            if self.header: # currently parsing the header
                if len(parts) > 1:
                    assert len(parts[0].strip()) == 0, f"Invalid header line number {self.lno}"
                    parse_hdr(self.info, parts[1])
                    continue
                if len(parts) == 1 and parts[0].strip() == "":
                    continue

                # This is not a header line - finalize
                # header parsing actions.
                if "UnixStartTime" in self.info:
                    self.start_time = float(self.info["UnixStartTime"])
                else:
                    assert False, "UnixStartTime is required."
                self.header = False

            if len(parts) == 1:
                tok = parts[0].split()
                if len(tok) == 0:
                    continue
                assert len(tok) == 18, f"Line {self.lno}: Invalid job line format."
                t0 = float(tok[1])
                assert t0 >= self.time, f"Line {self.lno}: Job submission times may only move forward."
                self.time = t0 # new lower bound
                for e in job_events(tok, self.start_time):
                    heappush(self.ebuf, e)

    def __iter__(self):
        return self

    def __next__(self) -> Event:
        while True:
            # Just return stuff from the event buffer.
            if self.eof and len(self.ebuf) == 0:
                raise StopIteration
            if self.eof or (len(self.ebuf) > 0 \
                        and self.ebuf[0].time <= self.time):
                return heappop(self.ebuf)

            self.prefetch()
