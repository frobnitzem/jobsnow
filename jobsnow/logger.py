from typing import Union
from enum import Enum
from pathlib import Path

import numpy as np

from .state import State
from .system import System, calc_user_stats, softmax
from .events import JobEvent, Job, Event

def to_str(x):
    if isinstance(x, Enum):
        return x.value
    return str(x)

state_columns = """
jobid user proj event time nodes
active_projs active_users
sys_rank sys_dt
sys_pend_jobs sys_run_jobs sys_pend_nodes sys_run_nodes
proj_rank proj_dt
proj_pend_jobs proj_run_jobs proj_pend_nodes proj_run_nodes
proj_frac proj_hrs
user_rank user_dt
user_pend_jobs user_run_jobs user_pend_nodes user_run_nodes
user_x user_y
is_leader
job_dt
""".split()
class LogSystem(System):
    events : dict[str, list]
    sys    : State

    def __init__(self, time : float, max_nodes : int) -> None:
        super().__init__(time, max_nodes)

        self.sys = State(time)
        self.events = dict((k,[]) for k in state_columns)

    def write(self, output : Union[str,Path]) -> None:
        with open(output, "w", encoding="utf-8") as f:
            keys = state_columns
            f.write(",".join(keys) + "\n")
            for x in zip( *[self.events[k] for k in keys] ):
                s = ",".join(map(to_str, x))
                f.write( s + "\n" )

    def add(self, event : Event) -> None:
        # Process the event and add it to the event log,
        #   `self.event`
        #
        # Replaces process_event, since this function calls
        # process_event during its operation.
        job = event.job
        self.events["jobid"].append(job.jobid)
        self.events["user"].append(job.user)
        self.events["proj"].append(job.proj)
        self.events["event"].append(event.name.value)
        self.events["time"].append(event.time)
        self.events["nodes"].append(job.nodes)
        self.events["active_projs"].append(self.active_projs())
        self.events["active_users"].append(self.active_users())

        sys = self.sys
        self.events["sys_pend_jobs"].append(len(sys.pend_jobs))
        self.events["sys_run_jobs"].append(
                             len(sys.jobs)-len(sys.pend_jobs))
        self.events["sys_pend_nodes"].append(sys.pend_nodes)
        self.events["sys_run_nodes"].append(sys.run_nodes)

        try:
            proj = self.projs[job.proj]
        except KeyError:
            if event.name != JobEvent.queued:
                raise
            proj = State()
            self.projs[job.proj] = proj
        self.events["proj_pend_jobs"].append(len(proj.pend_jobs))
        self.events["proj_run_jobs"].append(
                             len(proj.jobs)-len(proj.pend_jobs))
        self.events["proj_pend_nodes"].append(proj.pend_nodes)
        self.events["proj_run_nodes"].append(proj.run_nodes)

        try:
            user = self.users[job.user]
        except KeyError:
            if event.name != JobEvent.queued:
                raise
            user = State()
            self.users[job.user] = user
        self.events["user_pend_jobs"].append(len(user.pend_jobs))
        self.events["user_run_jobs"].append(
                             len(user.jobs)-len(user.pend_jobs))
        self.events["user_pend_nodes"].append(user.pend_nodes)
        self.events["user_run_nodes"].append(user.run_nodes)

        rank, dt = self.sys.process_event(event)
        self.time = event.time
        self.events["sys_rank"].append(rank)
        self.events["sys_dt"].append(dt)

        last_upd = job.last_update()
        if last_upd is None:
            dt = 0.0
        else:
            dt = event.time - last_upd
        self.events["job_dt"].append(dt)
        self.events["is_leader"].append(
                user.lead_jobid() == job.jobid)

        order, y, hrs = calc_user_stats(self.projs, self.time)
        i = order[job.proj]
        self.events["proj_hrs"].append(hrs[i])
        hrs /= (hrs.sum() + (hrs.sum() == 0))
        self.events["proj_frac"].append(hrs[i])

        order, y, hrs = calc_user_stats(self.users, self.time) # type: ignore[arg-type]
        x = softmax(y)
        i = order[job.user] # type: ignore[index]
        self.events["user_x"].append(x[i])
        self.events["user_y"].append(y[i])

        ans = self.process_event(event, False)
        if ans is None:
            rp, dtp, ru, dtu = 0, 0, 0, 0
        else:
            rp, dtp, ru, dtu = ans
        self.events["proj_rank"].append(rp)
        self.events["proj_dt"].append(dtp)

        self.events["user_rank"].append(ru)
        self.events["user_dt"].append(dtu)

# note: dt = time between last update and current
# jobid, user, proj, event, time,
#        sys_dt, sys_rank,
#        sys_pend_jobs, sys_run_jobs,
#        sys_pend_nodes, sys_run_nodes,
#
#        proj_dt, proj_rank,
#        proj_pend_jobs, proj_run_jobs,
#        proj_pend_nodes, proj_run_nodes,
#
#        user_dt, user_rank,
#        user_pend_jobs, user_run_jobs,
#        user_pend_nodes, user_run_nodes,
#
#        job_dt,
#        job_pend_jobs, job_run_jobs,
#        job_pend_nodes, job_run_nodes
#        
