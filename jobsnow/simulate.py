from typing import Optional
import bisect
import numpy as np

from .events import Job, Event, JobEvent
from .state import State
from .system import System
from .rates import draw_action, draw_sorted, draw_timedelta
from .draw_event import draw_new_job
from .draw_job import JobGenerator

Generator = np.random.Generator

# TODO: move the job_fates code into draw_job.JobGenerator
# Raw job statistics:
# success:  N = 12842 T = 7372.525277774141
# failed:   N = 2566  T = 1675.0588888868806
# killed:   N = 1952  T = 11595.412499999511
# canceled: N = 3096  T = 11990.673055560736
_Ntot = 12842+2566+1952

job_fate_cdf = np.array([12842/_Ntot, (12842+2566)/_Ntot, 1.0])
job_fates = {
    0: ( 12842/7372.525277774141, JobEvent.success),
    1: (  2566/1675.0588888868806, JobEvent.failed),
    2: (  1952/11595.412499999511, JobEvent.killed),
    3: (  3096/(7372.525277774141+11990.673055560736+1675.0588888868806+11595.412499999511), JobEvent.canceled),
}

def exp_cutoff_avg(k, t1=24.0):
    # Compute the average of the truncated exponential
    # distribution using the MGF:
    # M(z) = int_0^t1 k e^{(z-k) t} dt
    #      = k/(k-z) ( 1 - e^{(z-k) t1} )
    M0 = 1.0 - exp(-k*t1)
    M1 = M0/k - exp(-k*t1)
    return M1/M0

# Calculate the expected job lifetime
def avg_lifetime():
    cdf = 0
    ans = 0.0
    for i, cdf1 in enumerate(job_fate_cdf):
        p = cdf1 - cdf
        ans += p * exp_cutoff_avg(job_fates[i][0])
        cdf = cdf1
    return ans

def is_user_event(e : Event) -> bool:
    return e.name == JobEvent.queued

class SimulatedSystem(System):
    events : list[Event]

    def __init__(self, rng : Generator,
                 time, max_nodes, nuser=50):
        super().__init__(time, max_nodes)

        # upcoming events (job completions, etc.)
        self.events = []
        self.users  = dict((i,State(time))
                           for i in range(nuser))
        self.jobs   = {}
        self.step_user_event(rng)
        for _, u in self.users.items(): # synchronize watches.
            u.last_update = self.time

    def start_job(self, rng : Generator, job : Job):
        """ Simulate starting the given job for execution
            at the current time (`self.time`).
            
            This adds a "done" event into the `self.events` queue.
        """
        start = Event( JobEvent.running, self.time, job )
        self.process_event( start )
        # determine job fate
        rate, fate = job_fates[draw_sorted(rng, job_fate_cdf)]
        dt = min(24.0, draw_timedelta(rng, rate))
        ev = Event(fate, self.time + dt, job)
        self.add_event(ev)

    def add_event(self, e : Event):
        assert e.time >= self.time
        bisect.insort_right(self.events, e, \
                            key = lambda event: event.time)

    def next_time(self) -> Optional[float]:
        if len(self.events) == 0:
            return None
        return self.events[0].time

    def advance(self, rng : Generator, dt : float):
        """ Advance to the specified time.
            yields all events with event.time <= time.
        """

        t = self.time + dt
        i = 0
        while len(self.events) > i:
            event = self.events[i]
            if event.time > t:
                break
            i += 1
            self.time = event.time

            # process internal state change
            self.process_event(event)
            if is_user_event(event):
                # update self.events
                self.step_user_event(rng)

            # yield event
            yield event

        self.events = self.events[i:]
        self.time = t

    def step_user_event(self, rng : Generator):
        order = list( self.users.keys() )
        consts = [self.users[uid].time_const() \
                                for uid in order]
        dt, i = draw_action(rng, consts)
        uid = order[i]
        time = self.time + dt
        proj = str(uid) # FIXME
        e = draw_new_job(rng, uid, proj, time, self.max_nodes)
        self.add_event(e)

        # Add a cancellation event in case nothing happens
        # to this job.
        rate, fate = job_fates[3]
        dt = draw_timedelta(rng, rate)
        ev = Event(fate, self.time + dt, e.job)
        self.add_event(ev)
