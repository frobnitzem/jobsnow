# Job Snow

A library describing events that a job scheduler
needs to deal with.  Simulation functionality
is also included by `draw_*` functions.  These
draw events, as documented in [[docs/simulator.md]].

This library can thus be used both to determine
statistics of jobs originated by real users, as well
as simulate this behavior.

Some features:

* Tracks scheduler state (system and user-level) from
  a stream of time-ordered events.

* Tracks a moving average of:

  - user wait-time
  - user system utilization (node-hours)

* Rate process modeling for:

  - User behavior
  - Job termination statistics

## Installation

    pip install git+https://gitlab.com/frobnitzem/jobsnow@main

## Usage

You can import this code as a library.  Use of the API is loosely
document in the tests.

There is also a command-line interface to create workload models
from [swf files](https://www.cs.huji.ac.il/labs/parallel/workload/swf.html).

    jobsnow analyze <workload.swf>

You can also simulate user and scheduler behavior using:

    jobsnow simulate <model>

